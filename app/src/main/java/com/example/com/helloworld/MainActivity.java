package com.example.com.helloworld;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.com.helloworld.R.id.btn4;

public class MainActivity extends AppCompatActivity {

    Button button5;
    Button button4;

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button5 = (Button) findViewById(btn4);
        button4 = (Button) findViewById(R.id.btn5);

        textView = (TextView) findViewById(R.id.text2);

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intent = new Intent(MainActivity.this,PictureActivity.class);
                startActivity(intent);

            }

        });




        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                textView.setText(button5.getText() + "dd");

            }

        });



    }
}
